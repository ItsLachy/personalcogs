from redbot.core import checks, Config
import discord
from redbot.core import commands
from redbot.core.utils import mod
import asyncio
import datetime
import time

class Slowmode(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.data = Config.get_conf(self, identifier=42)
        default_guild = {
            "percent": 80,
            "toggle": False,
            "bypass": None
        }
        default_channel = {
            "slow": None,
            "vote": False,
            "imageonly": False
        }
        default_member = {
            "lastmsg": None,
            "msgdel": 0
        }
        self.data.register_member(**default_member)
        self.data.register_channel(**default_channel)
        self.data.register_guild(**default_guild)


    @commands.command()
    @checks.admin_or_permissions(ban_members=True)
    async def setbypassrole(self, ctx, role : discord.Role = None):
        await self.data.guild(ctx.guild).bypass.set(role.id)
        await ctx.send("Bypass role set !")

    async def can_bypass(self, member):
        try:
            if member.id == "99116871994859520" or await self.bot.is_owner(member):
                return True
            idmod = await self.data.guild(ctx.guild).bypass()
            modrole = discord.utils.get(member.guild.roles, id=idmod)
            if member.top_role >= modrole:
                return True
            return False
        except:
            return False

    async def ismodo(self, user):
        try:
            if user.id == "99116871994859520" or await self.bot.is_owner(user):
                return True
            idmod = await self.bot.db.guild(user.guild).mod_role()
            modrole = discord.utils.get(user.guild.roles, id=idmod)
            if user.top_role >= modrole:
                return True
            return False
        except:
            return False


    async def timer(self, user, channel, duration):
        while duration != 0:
            await asyncio.sleep(1)
            duration -= 1
        if duration == 0:
            await self.umute(user, channel)

    @commands.command()
    @checks.admin_or_permissions(kick_members=True)
    async def vote(self, ctx):
        if await self.data.channel(ctx.channel).vote():
            await self.data.channel(ctx.channel).vote.set(False)
            await ctx.send("Vote mode dÃ©sactivÃ©.")
        elif not await self.data.channel(ctx.channel).vote():
            await self.data.channel(ctx.channel).vote.set(True)
            await ctx.send("Vote mode activÃ©.")
        else:
            await ctx.send("Roboto tout cassÃ©.")

    @commands.command()
    @checks.admin_or_permissions(kick_members=True)
    async def imageonly(self, ctx):
        if await self.data.channel(ctx.channel).imageonly():
            await self.data.channel(ctx.channel).imageonly.set(False)
            await ctx.send("Image only dÃ©sactivÃ©.")
        elif not await self.data.channel(ctx.channel).imageonly():
            await self.data.channel(ctx.channel).imageonly.set(True)
            await ctx.send("Image only activÃ©.")
        else:
            await ctx.send("Roboto tout cassÃ©.")


    async def limiter(self, message):
        channel = message.channel
        author = message.author
        if await self.ismodo(author):
           return
        elif isinstance(message.channel, discord.channel.DMChannel):
            return
        else:
            if await self.data.guild(channel.guild).toggle():
                if await self.can_bypass(author):
                    pass
                else:
                    try:
                        if len(message.content) <= 5:
                            return
                        else:
                            cpt = 0
                            cptb = 0
                            for i in message.content:
                                if i.isupper():
                                    cptb += 1
                                elif i.lower() in "1234567890abcdefghijklmnopqrstuvwxyz .,!?/-":
                                    pass
                                else:
                                    cpt += 1
                        if ((cptb / len(message.content)) * 100) >= await self.data.guild(channel.guild).percent():
                            msg = "N'abuse pas des majuscules s'il te plait. Nous voyons trÃ¨s bien ton message en minuscule."
                            caps = await self.data.member(message.author).msgdel()
                            if caps == 0:
                                await self.data.member(message.author).msgdel.set(1)
                                await message.author.send(msg)
                            else:
                                await message.delete()
                                await message.author.send(msg)
                                await self.data.member(message.author).msgdel.set(caps + 1)
                                if caps + 1 > 20:
                                    await self.mutee(message.author, message.channel)
                                    await message.author.send("10 minutes de mute pour la spam de caps, enjoy !")
                                    await self.timer(message.author, message.channel, 600)
                                else:
                                    pass
                        else:
                            pass
                    except:
                        pass
            if await self.data.channel(channel).vote():
                await message.add_reaction('\U00002705')
                await message.add_reaction('\U0000274e')
            if await self.data.channel(channel).imageonly():
                if len(message.attachments) == 0:
                    await message.delete()
            if await self.data.channel(channel).slow() is None or await self.data.channel(channel).slow() == 0:
                return
            else:
                if await self.data.member(author).lastmsg() is None:
                    await self.data.member(author).lastmsg.set(time.time())
                    await self.data.member(author).msgdel.set(0)
                elif (time.time() - await self.data.member(author).lastmsg() <= await self.data.channel(channel).slow()):
                    await message.delete()
                    await self.data.member(author).lastmsg.set(time.time())
                    await self.data.member(author).msgdel.set(await self.data.member(author).msgdel() +1)
                else:
                    await self.data.member(author).lastmsg.set(time.time())
                if await self.data.member(author).msgdel() >= 10:
                    self.temp = self.bot.loop.create_task(self.timer(author, channel, 300))
                    await self.mutee(author, channel)
                    await self.data.member(author).msgdel.set(0)


                       